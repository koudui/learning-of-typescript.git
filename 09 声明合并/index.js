// 如果定义了名字相同的函数、接口、类，那么将会通过声明合并成为一个类型
function funcA(name) {
    return name;
}
;
// 属性在合并时需要保证属性的类型是相同的
// 类的合并
// class B {
//   name: string = '';
// }
// class B {
//   age: number = 0;
// }
