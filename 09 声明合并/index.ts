// 如果定义了名字相同的函数、接口，那么将会通过声明合并成为一个类型

// 函数的合并
// 定义多个同名的函数，会合并为一个函数类型，也就是函数重载
function funcA(name: number): number;
function funcA(name: string): string;
function funcA(name: string | number) : string | number {
  return name;
};

// 接口的合并
interface A {
  name: string;
}
interface A {
  age: number
}
// 等价于
interface A {
  name: string;
  age: number
}
// 属性在合并时需要保证属性的类型是相同的