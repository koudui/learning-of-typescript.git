// implements 实现接口
var CarA = /** @class */ (function () {
    function CarA() {
    }
    CarA.prototype.lightOn = function () { };
    ; // 实现类需要实现接口中所有的方法
    CarA.prototype.alert = function () { };
    ; // 实现类需要实现接口中所有的方法
    return CarA;
}());
var CarB = /** @class */ (function () {
    function CarB() {
    }
    CarB.prototype.lightOn = function () { };
    ; // 实现类需要实现接口中所有的方法
    CarB.prototype.alert = function () { };
    ; // 实现类需要实现接口中所有的方法
    return CarB;
}());
// 接口继承类
// 在几乎所有的面向对象中，接口是不能继承自类的，因为接口和类在定义上有很大的不同，但是在TypeScript中是可以的。如果难以理解，那我们就一步一步来
// 1. 首先，当我们在声明一个类时，除了会创建一个类对象，还会创建一个同名的类型
var Person = /** @class */ (function () {
    function Person(name) {
        this.name = name;
    }
    ;
    Person.prototype.getName = function () {
        return this.name;
    };
    ;
    Person.hello = function () { };
    ;
    // private sex: string = 'unkown';
    Person.age = 0;
    return Person;
}());
// 此时的Person作为一个类型约束，其不包含构造函数、静态属性和方法
function getUserName(p) {
    // new p(); // 编译错误，类型Person没有构造函数
    console.log('getUserName', p.getName());
}
getUserName(new Person('张三'));
function getUserName2(p) {
    // new p(); // 编译错误，类型Person没有构造函数
    console.log('getUserName2', p.getName());
}
getUserName2(new Person('李四'));
var Stu = /** @class */ (function () {
    function Stu() {
        this.addr = '';
        this.name = '';
    }
    Stu.prototype.getName = function () {
        return 'getName';
    };
    return Stu;
}());
// 4. 接口继承类，类中不应该有私有属性和私有方法，也不能有受保护的属性和方法
// 5. 最后是不建议使用接口实现类
