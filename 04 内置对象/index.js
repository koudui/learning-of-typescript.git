// js中有很多的内置对象，可以直接在ts中使用
// ES内置对象，例如Boolean、Date、RegExp等
var bool = new Boolean(1); // true
var date = new Date();
var regexp = /[ab]/;
// DOM和BOM的内置对象，例如Document、HTMLElement、Event等
var div = document.getElementsByTagName('div')[0];
// TypeScript和Node.js
// Node.js不是内置对象的一部分，如果要用ts写Node.js，则需要引入Node.js的声明文件
// npm install @types/node --save-dev
