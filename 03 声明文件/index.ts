// 当使用第三方库时，我们需要引入其声明文件，这样才可以获得代码补全、接口提示等功能

// 声明语句
// 使用declare var声明一个全局变量
declare let helloName: string;

// 声明文件
// 存放声明语句的文件，一般来说就是声明文件，以.d.ts为后缀名
// 一般来说，ts会解析所有的*.ts文件，当然也包括.d.ts文件
// 一些常用的第三方库有自己的声明文件，可以通过npm install的方式安装，可以现在页面搜索：https://www.typescriptlang.org/dt/search/

/*********************************书写声明文件 */
// 声明文件是以.d.ts结尾的，应该统一放在一个目录下
// 全局变量的声明主要有：declare var、declare function、declare class、declare enum、declare namespace、interface 和type

// declare var 声明全局变量
// 除了使用declare var，也可以使用declare let、declare const。使用declare const声明的是全局常量，是不能再修改的。
// 声明全局变量时，声明语句中只是定义类型，而不是具体的实现。
declare const userName: string;
// declare const userName: string = 'hello'; // 不能定义具体的实现

// declare function 声明全局函数
// 在声明全局函数的语句中，函数也是可以保持重载的。声明时也不能有具体的实现
declare function sayHello(): any
declare function sayHello(name: string): any

// declare class 声明全局类
declare class Animals {
  name: string;
  constructor(name: string);
  say(): void;
} // 只用定义类的结构，不能定义具体的实现

// declare enum 声明全局枚举
declare enum Color {
  Blue,
  White,
  Black,
  Red
}
let color = Color.Red;

// 混用declare和export，可将声明的多个变量一起导出
// export namespace，可用于导出一个拥有子属性的对象
export namespace foo {
  // const name: string;
}
// export default，可以导出一个默认值，在使用时就可以直接import而不是使用import {}的方式
// 只有function、class、interface可以默认导出，其他类型的变量需要先定义才能默认导出