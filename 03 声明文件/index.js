"use strict";
// 当使用第三方库时，我们需要引入其声明文件，这样才可以获得代码补全、接口提示等功能
exports.__esModule = true;
var color = Color.Red;
// export default，可以导出一个默认值，在使用时就可以直接import而不是使用import {}的方式
// 只有function、class、interface可以默认导出，其他类型的变量需要先定义才能默认导出
